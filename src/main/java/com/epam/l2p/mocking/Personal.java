package com.epam.l2p.mocking;

public class Personal {

    public void foodForAMonth(final int salary) {
        System.out.println("Calculating food cost for a month from salary " + salary);
    }


}
