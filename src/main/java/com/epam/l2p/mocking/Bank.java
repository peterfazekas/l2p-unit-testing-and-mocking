package com.epam.l2p.mocking;

public class Bank {

    public void loanRepayment(final int salary) {
        System.out.println("Calculating bank loan repayment from salary " + salary);
    }

    public void mortgageRepayment(final int salary) {
        System.out.println("Calculating mortgage repayment from salary " + salary);
    }

    public void pensionFund(final int salary) {
        System.out.println("Calculating pension fund from salary " + salary);
    }

}
