package com.epam.l2p.mocking;

public class SalaryCalculator {

    private final Tax tax;
    private final Bank bank;
    private final Personal personal;

    public SalaryCalculator(final Tax tax, final Bank bank, final Personal personal) {
        this.tax = tax;
        this.bank = bank;
        this.personal = personal;
    }

    public void calculate(final int salary) {
        tax.personalIncomeTax(salary);
        bank.loanRepayment(salary);
        bank.mortgageRepayment(salary);
        tax.spouseTax(salary);
        bank.pensionFund(salary);
        personal.foodForAMonth(salary);
    }
}
