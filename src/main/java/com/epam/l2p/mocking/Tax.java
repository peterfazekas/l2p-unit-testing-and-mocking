package com.epam.l2p.mocking;

public class Tax {

    public void personalIncomeTax(final int salary) {
        System.out.println("Calculating personal income tax from salary " + salary);
    }

    public void spouseTax(final int salary) {
        System.out.println("Calculating spouse tax from salary " + salary);
    }

}
