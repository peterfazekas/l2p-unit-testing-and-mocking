package com.epam.l2p.unittesting;

public class Calculate {

    public Integer add(final Integer n1, final Integer n2) {
        return isNotNull(n1, n2) ? Integer.sum(n1, n2) : 0;
    }

    private boolean isNotNull(Integer... numbers) {
        boolean notNull = true;
        for (Integer num : numbers) {
            notNull &= (num != null);
        }
        return notNull;
    }

}
