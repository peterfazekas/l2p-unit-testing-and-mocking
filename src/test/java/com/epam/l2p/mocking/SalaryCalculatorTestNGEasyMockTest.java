package com.epam.l2p.mocking;

import org.easymock.EasyMock;
import org.easymock.IMocksControl;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SalaryCalculatorTestNGEasyMockTest {

    private Tax tax;
    private Bank bank;
    private Personal personal;
    private SalaryCalculator underTest;
    private IMocksControl control;

    @BeforeMethod
    public void setUp() {
        control = EasyMock.createStrictControl();
        tax = control.createMock(Tax.class);
        bank = control.createMock(Bank.class);
        personal = control.createMock(Personal.class);
        underTest = new SalaryCalculator(tax, bank, personal);
        control.reset();
    }

    @Test
    public void testCalculateShouldLeadNoErrorWheMethodCallOrderIsCorrect() {
        // GIVEN
        int salary = 1000;
        tax.personalIncomeTax(salary);
        EasyMock.expectLastCall().times(1);
        bank.loanRepayment(salary);
        EasyMock.expectLastCall().times(1);
        bank.mortgageRepayment(salary);
        EasyMock.expectLastCall().times(1);
        tax.spouseTax(salary);
        EasyMock.expectLastCall().times(1);
        bank.pensionFund(salary);
        EasyMock.expectLastCall().times(1);
        personal.foodForAMonth(salary);
        EasyMock.expectLastCall().times(1);
        control.replay();

        // WHEN
        underTest.calculate(salary);

        // THEN
        control.verify();
    }

    @Test(expectedExceptions = AssertionError.class)
    public void testCalculateShouldLeadErrorWheMethodCallOrderIsNotCorrect() {
        // GIVEN
        int salary = 1000;
        bank.loanRepayment(salary);
        EasyMock.expectLastCall().times(1);
        tax.personalIncomeTax(salary);
        EasyMock.expectLastCall().times(1);
        bank.mortgageRepayment(salary);
        EasyMock.expectLastCall().times(1);
        tax.spouseTax(salary);
        EasyMock.expectLastCall().times(1);
        bank.pensionFund(salary);
        EasyMock.expectLastCall().times(1);
        personal.foodForAMonth(salary);
        EasyMock.expectLastCall().times(1);
        control.replay();

        // WHEN
        underTest.calculate(salary);

        // THEN
        control.verifyUnexpectedCalls();
    }

}