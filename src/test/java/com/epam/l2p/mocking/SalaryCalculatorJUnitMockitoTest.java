package com.epam.l2p.mocking;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class SalaryCalculatorJUnitMockitoTest {

    @Mock
    private Tax tax;
    @Mock
    private Bank bank;
    @Mock
    private Personal personal;
    @InjectMocks
    private SalaryCalculator underTest;

    @Test
    public void testCalculatorMethodCallsHappensInCorrectOrder() {
        // GIVEN
        int salary = 1000;
        InOrder inOrder = Mockito.inOrder(tax, bank, personal);
        // WHEN
        underTest.calculate(salary);

        // THEN
        inOrder.verify(tax).personalIncomeTax(salary);
        inOrder.verify(bank).loanRepayment(salary);
        inOrder.verify(bank).mortgageRepayment(salary);
        inOrder.verify(tax).spouseTax(salary);
        inOrder.verify(bank).pensionFund(salary);
        inOrder.verify(personal).foodForAMonth(salary);
        Mockito.verifyNoMoreInteractions(tax, bank, personal);
    }

}