package com.epam.l2p.unittesting;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CalculateJUnitOverflowCasesTest {

    private Calculate underTest;
    private Integer n1;
    private Integer n2;
    private Integer expectedResult;

    public CalculateJUnitOverflowCasesTest(final Integer n1, final Integer n2, final Integer expectedResult) {
        this.n1 = n1;
        this.n2 = n2;
        this.expectedResult = expectedResult;
    }

    @Before
    public void setUp() {
        underTest = new Calculate();
    }

    @Test
    public void testAddShouldGetCorrectValueWhenInputArgumentsWithingIntegerRange() {
        // GIVEN
        // WHEN
        Integer actual = underTest.add(n1, n2);

        // THEN
        Assert.assertEquals(expectedResult, actual);
    }

    @Parameterized.Parameters
    public static Collection primeNumbers() {
        return Arrays.asList(new Object[][] {
                {Integer.MAX_VALUE, 1, Integer.MIN_VALUE},
                {Integer.MIN_VALUE, -1, Integer.MAX_VALUE},
                {Integer.MAX_VALUE, Integer.MAX_VALUE, -2},
                {Integer.MIN_VALUE, Integer.MIN_VALUE, 0}
        });
    }

}