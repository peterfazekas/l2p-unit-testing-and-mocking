package com.epam.l2p.unittesting;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class CalculateJUnitNullCasesTest {

    private Calculate underTest;
    private Integer n1;
    private Integer n2;
    private Integer expectedResult;

    public CalculateJUnitNullCasesTest(final Integer n1, final Integer n2, final Integer expectedResult) {
        this.n1 = n1;
        this.n2 = n2;
        this.expectedResult = expectedResult;
    }

    @Before
    public void setUp() {
        underTest = new Calculate();
    }

    @Test
    public void testAddShouldGetCorrectValueWhenInputArgumentsWithingIntegerRange() {
        // GIVEN
        // WHEN
        Integer actual = underTest.add(n1, n2);

        // THEN
        Assert.assertEquals(expectedResult, actual);
    }

    @Parameterized.Parameters
    public static Collection primeNumbers() {
        return Arrays.asList(new Object[][] {
                {5, null, 0},
                {null, -5, 0},
                {null, null, 0}
        });
    }

}