package com.epam.l2p.unittesting;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CalculateTestNGTest {

    private Calculate underTest;

    @BeforeMethod
    public void setUp() {
        underTest = new Calculate();
    }

    @Test(dataProvider = "normalCases")
    public void testAddShouldGetCorrectValueWhenInputArgumentsWithingIntegerRange(final Integer n1, final Integer n2, final Integer expectedResult) {
        // GIVEN
        // WHEN
        Integer actual = underTest.add(n1, n2);

        // THEN
        Assert.assertEquals(actual, expectedResult);
    }

    @DataProvider(name = "normalCases")
    private Object[][] provideDataAndExpectedResultForAddMethodInNormalCases() {
        return new Object[][] {
                {0, 0, 0},
                {5, -5, 0},
                {-5, 5, 0},
                {0, 5, 5},
                {5, 0, 5},
                {0, -5, -5},
                {-5, 0, -5},
                {Integer.MAX_VALUE, 0, Integer.MAX_VALUE},
                {0, Integer.MAX_VALUE, Integer.MAX_VALUE},
        };
    }

    @Test(dataProvider = "nullCases")
    public void testAddShouldGetCorrectValueWhenInputArgumentsCanBeNull(final Integer n1, final Integer n2, final Integer expectedResult) {
        // GIVEN
        // WHEN
        Integer actual = underTest.add(n1, n2);

        // THEN
        Assert.assertEquals(actual, expectedResult);
    }

    @DataProvider(name = "nullCases")
    private Object[][] provideDataAndExpectedResultForAddMethodInNullCases() {
        return new Object[][] {
                {5, null, 0},
                {null, -5, 0},
                {null, null, 0}
        };
    }

    @Test(dataProvider = "OverflowCases")
    public void testAddShouldGetCorrectValueWhenInputArgumentsOverflowIntegerRange(final Integer n1, final Integer n2, final Integer expectedResult) {
        // GIVEN
        // WHEN
        Integer actual = underTest.add(n1, n2);

        // THEN
        Assert.assertEquals(expectedResult, actual);
    }

    @DataProvider(name = "OverflowCases")
    private Object[][] provideDataAndExpectedResultForAddMethodInOverflowCases() {
        return new Object[][] {
                {Integer.MAX_VALUE, 1, Integer.MIN_VALUE},
                {Integer.MIN_VALUE, -1, Integer.MAX_VALUE},
                {Integer.MAX_VALUE, Integer.MAX_VALUE, -2},
                {Integer.MIN_VALUE, Integer.MIN_VALUE, 0}
        };
    }

}