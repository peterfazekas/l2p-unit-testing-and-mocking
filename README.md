# Unit Testing and Mocking


## Write the unit tests covering all the test cases in JUnit, TestNG or any other framework. (1 point for each framework. JUnit is a must. Max: 2)

* Unit Tests for Integer addition method.
* Input: Integer, Integer
* Output: Integer or 0

## Write the unit tests using mocks, and testing the order of the calls using Mockito, EasyMock or any other framework. (1 point for each framework. One should be either Mockito or EasyMock. Max: 2)

* Unit Tests for salary calculator. Method takes the salary and calls the void methods.
* Input: int
* Output: void

### Calls in this order:
1. Tax.personalIncomeTax
2. Bank.loanRepayment
3. Bank.mortgageRepayment
4. Tax.spouseTax
5. Bank.pensionFund
6. Personal.foodForAMonth

This is the bare minimum, you can also experiment with Hamcrest, add fixtures, use parameterised tests, etc...